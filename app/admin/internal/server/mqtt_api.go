package server

import (
	"context"
	"fmt"

	"github.com/tx7do/kratos-transport/broker"

	"kratos-realtimemap/api/hfp"
)

func hfpEventCreator() broker.Any { return &hfp.Event{} }

type hfpEventHandler func(_ context.Context, topic string, headers broker.Headers, msg *hfp.Event) error

func registerSensorDataHandler(fnc hfpEventHandler) broker.Handler {
	return func(ctx context.Context, event broker.Event) error {
		switch t := event.Message().Body.(type) {
		case *hfp.Event:
			if err := fnc(ctx, event.Topic(), event.Message().Headers, t); err != nil {
				return err
			}
		default:
			return fmt.Errorf("unsupported type: %T", t)
		}
		return nil
	}
}
