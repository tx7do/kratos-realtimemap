package service

import (
	"context"
	"fmt"

	"google.golang.org/protobuf/types/known/emptypb"

	v1 "kratos-realtimemap/api/admin/v1"
	"kratos-realtimemap/app/admin/internal/pkg/data"
)

func (s *AdminService) ListOrganizations(_ context.Context, _ *emptypb.Empty) (*v1.ListOrganizationsReply, error) {
	reply := &v1.ListOrganizationsReply{
		Organizations: data.AllOrganizations.MapToBaseInfoArray(),
	}

	return reply, nil
}

func (s *AdminService) GetOrganization(_ context.Context, req *v1.GetOrganizationReq) (*v1.GetOrganizationReply, error) {
	if org, ok := data.AllOrganizations[req.OrgId]; ok {
		return &v1.GetOrganizationReply{
			Id:        org.Id,
			Name:      org.Name,
			Geofences: org.MapToGeofenceArray(),
		}, nil
	} else {
		return nil, v1.ErrorResourceNotFound(fmt.Sprintf("Organization %s not found", req.OrgId))
	}
}

func (s *AdminService) GetVehicleTrail(_ context.Context, req *v1.GetVehicleTrailReq) (*v1.GetVehicleTrailReply, error) {
	his := s.positionHistory.GetVehicleTrail(req.Id)
	if his == nil {
		return nil, v1.ErrorResourceNotFound(fmt.Sprintf("%s positions history not found", req.Id))
	}
	return &v1.GetVehicleTrailReply{Positions: his}, nil
}
